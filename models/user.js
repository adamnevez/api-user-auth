const Sequelize = require('sequelize');
const db = require('../config/database/db');

const Users = db.define('users',{

	id:{
		type: Sequelize.INTEGER,
		autoIncrement: true,
		allowNull: false,
		primaryKey: true, 
	},
	
	email: {
		type: Sequelize.STRING,
		allowNull: false, 
	},
	
	password: {
		type: Sequelize.STRING,
		allowNull: false,
	}
});

// create table
Users.sync();

module.exports = Users;