(async () => {
    const Devices = require('./models/device');
 
    try {
        const resultadoCreate = await Devices.create({
            color: 'white',
            partNumber: '3A443DFKz7DsA098PKL58',
            category_id: '1'
        })
        console.log(resultadoCreate);
    } catch (error) {
        console.log(error);
    }
})();

(async () => {
    const Category = require('./models/Category');
 
    try {
        const categoryCreate = await Category.create({
            name: 'Development',
        })
        console.log(categoryCreate);
    } catch (error) {
        console.log(error);
    }
})();
