const { db } = require('./config/database/db');
const express = require('express');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const app = express();

const Device = require('./models/device');
const Category = require('./models/category');
const User = require('./models/user');
const { query } = require('express');

app.use(express.json());

app.use((_req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "GET, PUT, POST,DELETE");
	res.header("Access-Control-Allow-Headers", "X-PINGOTHER, Content-Type, Authorization");
	app.use(cors());
	next();
});


function verifyToken(req, res) {

	if (!req.headers.authorization) {
		return res.status(401).send('Unauthorized request')
	}

	let token = req.headers.authorization.split(' ')[1]
	if (token === 'null') {
		return res.status(401).send('Unauthorized request')
	}

	let payload = jwt.verify(token, 'secretkey')
	if (!payload) {
		return res.status(401).send('Unauthorized request')
	}

	req.useId = payload.subject
	next()
}

// init endpoint users
app.get('/', verifyToken, async (_req, res) => {
	await User.findAll({ order: [['id', 'DESC']] }).then(function (users) {
		res.json({ users });
	});
});

app.post('/register', async (req, res) => {
	const resultRegsiter = await User.create(req.body)
		.then(function () {
			const payload = { subject: User._id }
			const token = jwt.sign(payload, 'secretkey')
			return res.status(200).json({
				error: false,
				token,
				message: "Usuario adicionado com sucesso."
			});
		}).catch(function (_error) {
			return res.status(401).json({
				error: true,
				message: "Erro ao adcionar Usuario"
			});
		});
});

app.post('/authorization', async (req, res) => {
	await User.findOne({ where: { email: req.body.email } })
		.then(users => {
			if (!users) {
				return res.status(401).json({
					error: true,
					message: 'Invalid email!'
				});
			} else {
				if (users.password !== req.body.password) {
					return res.status(401).json({
						error: true,
						message: 'Invalid password!'
					});
				} else {
					const payload = { subject: users._id }
					const token = jwt.sign(payload, 'secretkey')
					console.log(payload)
					return res.status(200).json({
						error: false,
						users,
						token
					});
				}
			}
		}).catch(function (_error) {
			return res.status(403).json({
				error: true,
				message: "Error email does not exist"
			});
		});
});

//init endpoint devices
app.get('/devices', async (_req, res) => {
	await Device.findAll({ order: [['id', 'DESC']] }).then(function (devices) {
		res.json({ devices });
	});
});

app.get('/devices/view/:id', async (req, res) => {
	await Device.findByPk(req.params.id)
		.then(devices => {
			return res.json({
				error: false,
				devices
			});
		}).catch(function (_erro) {
			return res.status(400).json({
				error: true,
				message: "Erro nao existe"
			});
		});
});

app.post('/device/create', async (req, res) => {
	const resultCad = await Device.create(
		req.body
	).then(function () {
		return res.json({
			error: false,
			message: "Device adicionado com sucesso."
		})
	}).catch(function (_erro) {
		return res.status(400).json({
			error: true,
			message: "Erro ao adcionar device"
		});
	});
});

app.put('/device/edit', async (req, res) => {
	await sleep(3000);
	function sleep(ms) {
		return new Promise((resolve) => {
			setTimeout(resolve, ms);
		});
	}
	await Device.update(req.body, {
		where: { id: req.body.id }
	}).then(function () {
		return res.json({
			error: false,
			message: "Device editado com sucesso"
		});
	}).catch(function (_erro) {
		return res.status(400).json({
			error: true,
			message: "Erro ao editar device"
		});
	});
});

app.delete('/device/delete/:id', async (req, res) => {
	await Device.destroy({
		where: { id: req.params.id }
	}).then(function () {
		return res.json({
			error: false,
			message: "Device deletado"
		});
	}).catch(function (_erro) {
		return res.status(400).json({
			error: true,
			message: "Erro ao deletar"
		});
	});
});

//init endpoint catoregory
app.get('/category/', async (_req, res) => {
	await Category.findAll({ order: [['id', 'ASC']] }).then(function (categories) {
		res.json({ categories });
	});
});

app.post('/category/create', async (req, res) => {
	const resultCategory = await Category.create(
		req.body
	).then(function () {
		return res.json({
			error: false,
			message: "Categoria adicionada com sucesso."
		})
	}).catch(function (_erro) {
		return res.status(400).json({
			error: true,
			message: "Erro ao adcionar categoria"
		});
	});
});

app.delete('/category/delete/:id', async (req, res) => {
	await Category.destroy({
		where: { id: req.params.id }
	}).then(function () {
		return res.json({
			error: false,
			message: "Categoria deletada"
		});
	}).catch(function (_erro) {
		return res.status(400).json({
			error: true,
			message: "Erro ao deletar"
		});
	});
});


app.listen(8080, function () {
	console.log("Serve start in http://localhost:8080/");
});