const { Sequelize } = require('sequelize');

require('dotenv').config();

const dbHost = process.env.DB_HOST;
const dbName = process.env.DB_NAME;
const dbUser = process.env.DB_USER;
const dbPassword = process.env.DB_PASSWORD;

const sequelize = new Sequelize(dbName,dbUser, dbPassword, {
  host: dbHost,
  dialect: 'mysql'
});

sequelize.authenticate()
.then(function(){
	console.log("Conection success!");
}).catch(function(err){
	console.log("Erro: Conection no success!"+dbName+'us:'+dbUser+'ps:'+dbPassword);	
});

module.exports = sequelize;